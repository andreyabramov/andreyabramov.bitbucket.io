========
Overview
========

This section outlines the step needed for basic processing of the raw output. The more advanced users may want to look at section :doc:`poweruser` for information on how to use the package for custom processing.

.. _data_structure:

Data Structure
--------------

This analysis package makes some assumptions about how the output from the simulation is structured. In the normal simulation workflow multiple individual jobs are executed in parallel over a batch system like HTCondor. All of the needed input for the simulation is stored in a directory called ``clean_input`` and each individual job has its own directory in the output called ``run_xxxxx`` where ``xxxxx`` is a number between 1 and 10000. There are submission scripts available in the coupling repository and also in this package that prepare the directory structure and run the jobs.

Because of the batch submission mechanics and the input being copied around to different nodes for every job, the files in the ``clean_input`` directory should only be the strictly necessary ones for the simulation in order to reduce network traffic. This means that auxillary files must be provided to the analysis routines separately. Such additional files are the MADX TFS file which is used to synchronise the S coordinate between SixTrack and MADX and the collimator settings file. In the default mode of operation the processing and analysis routines would look for auxiliary files in a directory called ``input`` inside the study directory. The default behaviour can be overridden, but experience shows that using the standard directory layout makes working with multiple studies more manageable and reduces cross-study pollution.

The results from the postporcessing of the data are written to a drirectory called ``output`` in the study directory. The processing performed is explained in more detail in the next section :ref:`processing`. The standard layout of directories in a study directory is depicted in :numref:`fig_data_layout`. 

.. _fig_data_layout:

.. figure:: figures/output_structure.png
   :scale: 20 %
   :alt: data layout

   The standard layout of a study directory containing the simulation input, the simulation output data, the auxiliary input for the processing and the processed output.

.. _processing:

Processing
----------

The first step in the analysis is the compression step. In this step the data from all the individual run directories is processed and merged into a single data set. The compression includes adding up the energy lost in the collimators, concatenating and binning the aperture losses and other merging operations that depend on the data present in the ``run_xxxxx`` directories and the preference of the user.

Before the actual processing of the data can begin, information is gathered from the main and auxillary input files. This information includes the type of machine, names and positions of collimators and the S-coordinate offset between MADX and SixTrack. To collect this information from the different sources and use it in the analysis the :class:`pyflusix.machine_parameters.MachineParameters` class is used. At the compression stage an instance of that class computes the needed parameters and writes them out to a file called ``machine_info.txt``. This file is a simple plain-text list of parameters that can later be loaded directly into another instance of the same class. This method of access allows universal availability of common machine parameters. The flow of common machine information is shown in :numref:`fig_machine_info`.

.. _fig_machine_info:

.. figure:: figures/machine_info.png
   :scale: 20 %
   :alt: machine info chart

   Chart of machine information shared between different analysis stages.

The compressed data is stored in its native reference frames - the aperture losses follow the S coordinate from SixTrack and the collimator losses are listed in terms of collimator id instead of an S position. The matching of S reference frames happens only when the compressed data is being loaded for use. This ensures the scalability and sanity of the processing - e.g. if the S-transform fails due to problems with the TFS file the compressed data is not corrupted.


Results
-------

Following the processing, the following core data sets are available:

+-----------------------+---------------------------+
| Data                  | Description               |
+=======================+===========================+
| Binned                | Aperture losses that have |
| aperture              | been binned for use in    |
| losses                | lossmap plotting.         |
| ``fort.999.comp``     |                           |
+-----------------------+---------------------------+
| Merged                | Aperture losses that have |
| aperture              | been simply merged from   |
| losses                | the individual runs into  |
| ``fort.999``          | a single file.            |
+-----------------------+---------------------------+
| Total                 | Collimator losses that    |
| collimator            | have been summed up from  |
| losses                | different runs            |
| ``fort.208``          |                           |
+-----------------------+---------------------------+


And using the provided flags additionally one can also obtain:

+------------------------+---------------------------+
| Data                   | Description               |
+========================+===========================+
| Aperture               | Additional information    |
| impacts                | about aperture losses     |
| ``fort.impacts``       | like transverse           |
|                        | coordinates, A, Z and     |
|                        | parent particle           |
|                        | (only if specified)       |
|                        |                           |
+------------------------+---------------------------+
| Fluka                  | A merged list of all      |
| particle generation    | particles produced by     |
| information            | Fluka with parent         |
| ``fluka_isotopes.log`` | particle, turn etc.       |
+------------------------+---------------------------+

There are tools to merge other simulation output types such as
trajectory dumps, collimator first touches etc. from the
individual runs, but because they very specific are not suitable
for data reduction they are currently not included in
the the standard compression flow and must be performed
separately if desired.
