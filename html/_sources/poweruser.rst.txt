===========
Power Users
===========


This is additional information for advanced users who want to utilise the funcionallity of the package in their own scripts.

.. _getting_started:

Getting Started
---------------

The executable scripts in the repository provide examples of how the different modules are used. In addition to this the module documentation :ref:`module_contents` provides information about the available features.


Interactive python session often help with trying some functionality out. In an interactive python session (like ipython) one can easily access the function signatures and docstrings.

.. testcode::

   >>> import pyflusix
   >>> pyflusix.aperture_losses.ApertureLosses

.. testoutput::

   Init signature: pyflusix.aperture_losses.ApertureLosses(self, filename, s_min=None, s_max=None,
                   s_start=None, s_total=None, verbose=True)
   Docstring:      Class to handle the loading and the processing of aperture impacts.
   File:           ~/physics/coderepos/pyflusix/aperture_losses.py
   Type:           type


