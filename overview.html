
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Overview &#8212; pyflusix 0.2.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/sphinxdoc.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Quickstart" href="quickstart.html" />
    <link rel="prev" title="Authorship" href="authorship.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="quickstart.html" title="Quickstart"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="authorship.html" title="Authorship"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">pyflusix 0.2.0 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Overview</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="overview">
<h1>Overview<a class="headerlink" href="#overview" title="Permalink to this headline">¶</a></h1>
<p>This section outlines the step needed for basic processing of the raw output. The more advanced users may want to look at section <a class="reference internal" href="poweruser.html"><span class="doc">Power Users</span></a> for information on how to use the package for custom processing.</p>
<div class="section" id="data-structure">
<span id="id1"></span><h2>Data Structure<a class="headerlink" href="#data-structure" title="Permalink to this headline">¶</a></h2>
<p>This analysis package makes some assumptions about how the output from the simulation is structured. In the normal simulation workflow multiple individual jobs are executed in parallel over a batch system like HTCondor at CERN. All of the needed input for the simulation is stored in a directory called <code class="docutils literal notranslate"><span class="pre">clean_input</span></code> and each individual job has its own directory in the output called <code class="docutils literal notranslate"><span class="pre">run_xxxxx</span></code> where <code class="docutils literal notranslate"><span class="pre">xxxxx</span></code> is a number between 1 and 99999. There are submission scripts available in the coupling repository and also in this package that prepare the directory structure and run the jobs.</p>
<p>Because of the batch submission mechanics and the input being copied around to different nodes for every job, the files in the <code class="docutils literal notranslate"><span class="pre">clean_input</span></code> directory should only be the strictly necessary ones for the simulation in order to reduce network traffic. This means that auxillary files must be provided to the analysis routines separately. Such additional files are the MADX .tfs file, which is used to synchronise the S coordinate between SixTrack and MADX, and the collimator settings file.</p>
<p>In the default mode of operation the processing and analysis routines would look for auxiliary files in a directory called <code class="docutils literal notranslate"><span class="pre">input</span></code> inside the study directory. The default behaviour can be overridden, but experience shows that using the standard directory layout makes working with multiple studies more manageable and reduces cross-study pollution.
The results from the post-processing of the data are written to a directory called <code class="docutils literal notranslate"><span class="pre">output</span></code> in the study directory. The processing performed is explained in more detail in the next section <a class="reference internal" href="#processing"><span class="std std-ref">Processing</span></a>. The standard layout of directories in a study directory is depicted in <a class="reference internal" href="#fig-data-layout"><span class="std std-numref">Fig. 1</span></a>.</p>
<div class="figure align-default" id="id3">
<span id="fig-data-layout"></span><a class="reference internal image-reference" href="_images/output_structure.png"><img alt="data layout" src="_images/output_structure.png" style="width: 100%;" /></a>
<p class="caption"><span class="caption-number">Fig. 1 </span><span class="caption-text">The standard layout of a study directory containing the simulation input, the simulation output data, the auxiliary input for the processing and the processed output.</span><a class="headerlink" href="#id3" title="Permalink to this image">¶</a></p>
</div>
</div>
<div class="section" id="processing">
<span id="id2"></span><h2>Processing<a class="headerlink" href="#processing" title="Permalink to this headline">¶</a></h2>
<p>The first step in the analysis is the compression step. In this step the data from all the individual run directories is processed and merged into a single data set. The compression includes adding up the energy lost in the collimators, concatenating and binning the aperture losses and other merging operations that depend on the data present in the <code class="docutils literal notranslate"><span class="pre">run_xxxxx</span></code> directories and the preference of the user.</p>
<p>Before the actual processing of the data can begin, information is gathered from the main and auxillary input files. This information includes the type of machine, name and position of collimators and the S-coordinate offset between MADX and SixTrack. To collect this information from the different sources and use it in the analysis the <a class="reference internal" href="modules.html#pyflusix.machine_parameters.MachineParameters" title="pyflusix.machine_parameters.MachineParameters"><code class="xref py py-class docutils literal notranslate"><span class="pre">pyflusix.machine_parameters.MachineParameters</span></code></a> class is used. At the compression stage an instance of that class computes the needed parameters and writes them out to a file called <code class="docutils literal notranslate"><span class="pre">machine_info.txt</span></code>. This file is a simple plain-text list of parameters that can later be loaded directly into another instance of the same class. This method of access allows universal availability of common machine parameters. The flow of common machine information is shown in <a class="reference internal" href="#fig-machine-info"><span class="std std-numref">Fig. 2</span></a>.</p>
<div class="figure align-default" id="id4">
<span id="fig-machine-info"></span><a class="reference internal image-reference" href="_images/machine_info.png"><img alt="machine info chart" src="_images/machine_info.png" style="width: 100%;" /></a>
<p class="caption"><span class="caption-number">Fig. 2 </span><span class="caption-text">Chart of machine information shared between different analysis stages.</span><a class="headerlink" href="#id4" title="Permalink to this image">¶</a></p>
</div>
<p>The compressed data is stored in its native reference frames - the aperture losses follow the S coordinate from SixTrack and the collimator losses are listed in terms of collimator id instead of an S position. The matching of S reference frames happens only when the compressed data is being loaded for use. This ensures the scalability and sanity of the processing - e.g. if the S-transform fails due to problems with the TFS file the compressed data is not corrupted.</p>
</div>
<div class="section" id="results">
<h2>Results<a class="headerlink" href="#results" title="Permalink to this headline">¶</a></h2>
<p>Following the processing, the following core data sets are available:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 46%" />
<col style="width: 54%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Data</p></th>
<th class="head"><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>Binned
aperture
losses
<code class="docutils literal notranslate"><span class="pre">fort.999.comp</span></code></p></td>
<td><p>Aperture losses that have
been binned for use in
lossmap plotting.</p></td>
</tr>
<tr class="row-odd"><td><p>Merged
aperture
losses
<code class="docutils literal notranslate"><span class="pre">fort.999</span></code></p></td>
<td><p>Aperture losses that have
been simply merged from
the individual runs into
a single file.</p></td>
</tr>
<tr class="row-even"><td><p>Total
collimator
losses
<code class="docutils literal notranslate"><span class="pre">fort.208</span></code></p></td>
<td><p>Collimator losses that
have been summed up from
different runs</p></td>
</tr>
</tbody>
</table>
<p>And using the provided flags additionally one can also obtain:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 47%" />
<col style="width: 53%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Data</p></th>
<th class="head"><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>Aperture
impacts
<code class="docutils literal notranslate"><span class="pre">fort.impacts</span></code></p></td>
<td><p>Additional information
about aperture losses
like transverse
coordinates, A, Z and
parent particle
(only if specified)</p></td>
</tr>
<tr class="row-odd"><td><p>Fluka
particle generation
information
<code class="docutils literal notranslate"><span class="pre">fluka_isotopes.log</span></code></p></td>
<td><p>A merged list of all
particles produced by
Fluka with parent
particle, turn etc.</p></td>
</tr>
</tbody>
</table>
<p>There are tools to merge other simulation output types such as
trajectory dumps, collimator first touches etc. from the
individual runs, but because they are very specific and not suitable
for data reduction, they are currently not included in
the the standard compression flow and must be performed
separately if desired.</p>
</div>
</div>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Overview</a><ul>
<li><a class="reference internal" href="#data-structure">Data Structure</a></li>
<li><a class="reference internal" href="#processing">Processing</a></li>
<li><a class="reference internal" href="#results">Results</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="authorship.html"
                        title="previous chapter">Authorship</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="quickstart.html"
                        title="next chapter">Quickstart</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/overview.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="quickstart.html" title="Quickstart"
             >next</a> |</li>
        <li class="right" >
          <a href="authorship.html" title="Authorship"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">pyflusix 0.2.0 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Overview</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright .
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.1.1.
    </div>
  </body>
</html>